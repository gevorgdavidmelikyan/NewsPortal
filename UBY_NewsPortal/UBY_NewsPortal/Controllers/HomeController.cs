﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;
using UBY_NewsPortal.Data;
using UBY_NewsPortal.Models;
using UBY_NewsPortal.Models.ManageViewModels;
using UBY_NewsPortal.Models.NewsViewModels;
using UBY_NewsPortal.Readers;

namespace UBY_NewsPortal.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            RssReaderToList myListRss = new RssReaderToList();
            List<SpyNews> spyNews = myListRss.GetRssData("https://www.rt.com/rss/usa/");
            ViewData["SpyNews"] = spyNews;
            return View(_context.SpyNews.ToList());
        }
        #region comingSoon
        public async Task<IActionResult> About(int page = 1)
        {
            //  ViewData["Message"] = "Your application description page.";
            int pageSize = 3;   // количество элементов на странице
            //var news = new News();
            //news.MyNews = _context.MyNews;
            IQueryable<News> source = _context.News.Where(c => c.Charter == Charter.Political).OrderByDescending(m=>m.PubDate);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                News = items
            };
            return View(viewModel);

            // return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        public IActionResult Travel()
        {
            ViewData["Message"] = "Coming Soon Travel Page";

            return View();
        }
        public IActionResult Space()
        {
            ViewData["Message"] = "Coming Soon Travel Page";

            return View();
        }
        public IActionResult Weather()
        {
            return View();
        }
        #endregion
        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> Political(int page = 1)
        {
            //Work incorrect
            RssReaderToList myListRss = new RssReaderToList();
            var qry = _context.SpyNews.Where(c => c.Charter == Charter.Political).AsNoTracking().OrderBy(p => p.PubDate);
            var model = await PagingList.CreateAsync(qry, 10, page);
            return View(model);
        }
        public async Task<IActionResult> Economics(int page = 1)
        {
            //RssReaderToList myListRss = new RssReaderToList();
            //List<SpyNews> spyNews = myListRss.GetRssData("https://www.azatutyun.am/api/zitvyejotv");
            //ViewData["SpyNews"] = spyNews;
            //return View(await _context.MyNews.Where(c => c.Charter == Charter.Economic).ToListAsync());
            int pageSize = 3;   // количество элементов на странице

            IQueryable<SpyNews> source = _context.SpyNews.Where(c => c.Charter == Charter.Economic);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                SpyNews = items
            };
            return View(viewModel);
        }
        public async Task<IActionResult> Sport()
        {
            RssReaderToList myListRss = new RssReaderToList();
            List<SpyNews> spyNews = myListRss.GetRssData("https://www.azatutyun.am/api/zkrjiqemujio");
            ViewData["SpyNews"] = spyNews;
            return View(await _context.MyNews.Where(c => c.Charter == Charter.Sport).ToListAsync());
        }

        public async Task<IActionResult> Science(int page = 1)
        {
            //  ViewData["Message"] = "Your application description page.";
            int pageSize = 3;   // количество элементов на странице
            //var news = new News();
            //news.MyNews = _context.MyNews;
            IQueryable<News> source = _context.News.Where(c => c.Charter == Charter.Science).OrderByDescending(m => m.PubDate);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                News = items
            };
            return View(viewModel);

            // return View();
        }
        public async Task<IActionResult> Culture(int page = 1)
        {
            //  ViewData["Message"] = "Your application description page.";
            int pageSize = 3;   // количество элементов на странице
            //var news = new News();
            //news.MyNews = _context.MyNews;
            IQueryable<News> source = _context.News.Where(c => c.Charter == Charter.Culture).OrderByDescending(m => m.PubDate);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                News = items
            };
            return View(viewModel);

            // return View();
        }
        public async Task<IActionResult> Society(int page = 1)
        {
            //  ViewData["Message"] = "Your application description page.";
            int pageSize = 3;   // количество элементов на странице
            //var news = new News();
            //news.MyNews = _context.MyNews;
            IQueryable<News> source = _context.News.Where(c => c.Charter == Charter.Society).OrderByDescending(m => m.PubDate);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                News = items
            };
            return View(viewModel);

            // return View();
        }
        public async Task<IActionResult> Armenia(int page = 1)
        {
            //  ViewData["Message"] = "Your application description page.";
            int pageSize = 3;   // количество элементов на странице
            //var news = new News();
            //news.MyNews = _context.MyNews;
            IQueryable<News> source = _context.News.Where(c => c.Charter == Charter.Armenia).OrderByDescending(m => m.PubDate);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                News = items
            };
            return View(viewModel);

            // return View();
        }
        public async Task<IActionResult> World(int page = 1)
        {
            //  ViewData["Message"] = "Your application description page.";
            int pageSize = 3;   // количество элементов на странице
            //var news = new News();
            //news.MyNews = _context.MyNews;
            IQueryable<News> source = _context.News.Where(c => c.Charter == Charter.World).OrderByDescending(m => m.PubDate);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                News = items
            };
            return View(viewModel);

            // return View();
        }
        public async Task<IActionResult> Region(int page = 1)
        {
            //  ViewData["Message"] = "Your application description page.";
            int pageSize = 3;   // количество элементов на странице
            //var news = new News();
            //news.MyNews = _context.MyNews;
            IQueryable<News> source = _context.News.Where(c => c.Charter == Charter.Region).OrderByDescending(m => m.PubDate);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                News = items
            };
            return View(viewModel);

            // return View();
        }
        public async Task<IActionResult> Artsakh(int page = 1)
        {
            //  ViewData["Message"] = "Your application description page.";
            int pageSize = 3;   // количество элементов на странице
            //var news = new News();
            //news.MyNews = _context.MyNews;
            IQueryable<News> source = _context.News.Where(c => c.Charter == Charter.Artsakh).OrderByDescending(m => m.PubDate);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                News = items
            };
            return View(viewModel);

            // return View();
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        // GET: MyNews/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var myNews = await _context.MyNews
                .SingleOrDefaultAsync(m => m.Id == id);
            if (myNews == null)
            {
                return NotFound();
            }

            return View(myNews);
        }
        public async Task<IActionResult> OnGetAsync(string searchString)
        {
           // searchString = "Փաշինյան";
            var news = from m in _context.News
                       select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                news = news.Where(s => s.Title.Contains(searchString));
            }

           ViewData["News"] = await news.ToListAsync();
            return View();
        }
    }
}
