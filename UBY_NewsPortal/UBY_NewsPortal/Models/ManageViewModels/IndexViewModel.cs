﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using UBY_NewsPortal.Models.NewsViewModels;

namespace UBY_NewsPortal.Models.ManageViewModels
{
    public class IndexViewModel
    {
        public string Username { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        public string StatusMessage { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public IEnumerable<SpyNews> SpyNews { get; set; }
        public IEnumerable<MyNews> MyNews { get; set; }
        public IEnumerable<News> News { get; set; }

    }
}
