﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UBY_NewsPortal.Data;
using UBY_NewsPortal.Models.NewsViewModels;

namespace UBY_NewsPortal.Controllers
{

   // [Authorize(Roles = "Admin")]
   [Authorize]
    public class NewsMakeController : Controller
    {
        private readonly ApplicationDbContext _context;
        public NewsMakeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()
        {
            return View(await _context.MyNews.ToListAsync());
        }
        public ActionResult RichTextEditorFeatures()
        {
            ViewBag.tools = new[] { "Bold", "Italic", "Underline", "StrikeThrough",
                "FontName", "FontSize", "FontColor", "BackgroundColor",
                "LowerCase", "UpperCase", "|",
                "Formats", "Alignments", "OrderedList", "UnorderedList",
                "Outdent", "Indent", "|",
                "CreateLink", "Image", "|", "ClearFormat", "Print",
                "SourceCode", "FullScreen", "|", "Undo", "Redo" };
            return View();
        }

        // GET: MyNews/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MyNews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,PubDate,Content,Charter")] MyNews myNews)
        {
            if (ModelState.IsValid)
            {
                _context.Add(myNews);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(myNews);
        }

        // GET: MyNews/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var myNews = await _context.MyNews.FindAsync(id);
            if (myNews == null)
            {
                return NotFound();
            }
            return View(myNews);
        }

        // POST: MyNews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,PubDate,Content,Charter")] MyNews myNews)
        {
            if (id != myNews.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(myNews);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MyNewsExists(myNews.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(myNews);
        }

        // GET: MyNews/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var myNews = await _context.MyNews
                .FirstOrDefaultAsync(m => m.Id == id);
            if (myNews == null)
            {
                return NotFound();
            }

            return View(myNews);
        }

        // POST: MyNews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var myNews = await _context.MyNews.FindAsync(id);
            _context.MyNews.Remove(myNews);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var myNews = await _context.MyNews
                .FirstOrDefaultAsync(m => m.Id == id);
            if (myNews == null)
            {
                return NotFound();
            }

            return View(myNews);
        }
        private bool MyNewsExists(int id)
        {
            return _context.MyNews.Any(e => e.Id == id);
        }
    }
}