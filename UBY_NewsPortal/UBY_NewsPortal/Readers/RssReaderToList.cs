﻿using UBY_NewsPortal.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using UBY_NewsPortal.Models.NewsViewModels;

namespace UBY_NewsPortal.Readers
{
    public class RssReaderToList
    {
        string parseFormat = "ddd, dd MMM yyyy HH:mm:ss zzz";
        public List<SpyNews> GetRssData(string chanel)
        {
            List<SpyNews> partnerNews = new List<SpyNews>();

            WebRequest myRequest = WebRequest.Create(chanel);
            WebResponse myResponse = myRequest.GetResponse();
            Stream rssStream = myResponse.GetResponseStream();
            XmlDocument rssDoc = new XmlDocument();
            rssDoc.Load(rssStream);
            XmlNodeList rssItems = rssDoc.SelectNodes("rss/channel/item");
            for (int i = 0; i < rssItems.Count; i++)
            {
                SpyNews news = new SpyNews();
                XmlNode rssNode;

                rssNode = rssItems.Item(i).SelectSingleNode("title");
                if (rssNode != null)
                    news.Title = rssNode.InnerText;
                else
                    news.Title = "";

                rssNode = rssItems.Item(i).SelectSingleNode("description");
                if (rssNode != null)
                    news.Description = rssNode.InnerText;
                else
                    news.Description = "";

                rssNode = rssItems.Item(i).SelectSingleNode("link");
                if (rssNode != null)
                    news.Link = rssNode.InnerText;
                else
                    news.Link = "";

                rssNode = rssItems.Item(i).SelectSingleNode("pubDate");
                if (rssNode != null)
                {
                    string x = rssNode.InnerText;
                    news.PubDate = DateTime.ParseExact(x, parseFormat,
                                            CultureInfo.InvariantCulture);
                }
                partnerNews.Add(news);
            }

            //myResponse.Close();
            //rssStream.Close();
            return partnerNews;
        }
    }
}
