﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UBY_NewsPortal.Models.NewsViewModels;
using UBY_NewsPortal.Readers;

namespace UBY_NewsPortal.Data
{
    public class SampleData
    {
        public static void Initialize(ApplicationDbContext context)
        {
            RssReaderToList myListRss = new RssReaderToList();
            List<SpyNews> my= new List<SpyNews>();
            if (!context.SpyNews.Any())
            {
                
                my = myListRss.GetRssData("https://www.azatutyun.am/api/zqtvmekot_");
                foreach (var item in my)
                {
                    context.SpyNews.AddRange(
                        new SpyNews
                        {
                            Link = item.Link,
                            Description = item.Description,
                            Title = item.Title,
                            PubDate = item.PubDate,
                            Charter = Charter.Political
                        });
                    context.SaveChanges();
                }
                my.Clear();
                my = myListRss.GetRssData("https://www.azatutyun.am/api/zitvyejotv");
                foreach (var item in my)
                {
                    context.SpyNews.AddRange(
                        new SpyNews
                        {
                            Link = item.Link,
                            Description = item.Description,
                            Title = item.Title,
                            PubDate = item.PubDate,
                            Charter = Charter.Economic
                        });
                    context.SaveChanges();
                }
                my.Clear();
                my = myListRss.GetRssData("https://www.azatutyun.am/api/zkrjiqemujio");
                foreach (var item in my)
                {
                    context.SpyNews.AddRange(
                        new SpyNews
                        {
                            Link = item.Link,
                            Description = item.Description,
                            Title = item.Title,
                            PubDate = item.PubDate,
                            Charter = Charter.Economic
                        });
                    context.SaveChanges();
                }

            }
        }
        
    }
}
