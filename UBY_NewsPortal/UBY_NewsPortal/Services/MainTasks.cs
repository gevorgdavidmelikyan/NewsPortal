﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UBY_NewsPortal.Data;

using Microsoft.Extensions.DependencyInjection;
using UBY_NewsPortal.Models;
using Microsoft.AspNetCore.Identity;
using UBY_NewsPortal.Readers;
using UBY_NewsPortal.Models.NewsViewModels;

namespace UBY_NewsPortal.Services
{
    public class MainTasks
    {
    }
    public class SheduledTaskService
    {
        private readonly ILogger _logger;
        ApplicationDbContext _context;
        IHostingEnvironment _env;
        UserManager<ApplicationUser> _userManager;

        static int i = 0;

        public SheduledTaskService(ILogger<SheduledTaskService> logger,
            ApplicationDbContext context,
            //  JobShedule jobShedule,
            IHostingEnvironment env
            //UserManager<ApplicationUser> userManager,
            //IParamProvider paramProvider
            )
        {
            _logger = logger;
            _context = context;
            //  _jobShedule = jobShedule;
            _env = env;
            //  _userManager = userManager;
            //  _paramProvider = paramProvider;
        }


        public async Task DoWork()
        {
            // System.Diagnostics.Debug.WriteLine(DateTime.Now);
            RssReaderToList myListRss = new RssReaderToList();
            List<SpyNews> my = new List<SpyNews>();


            my = myListRss.GetRssData("https://www.azatutyun.am/api/zqtvmekot_");
            foreach (var item in my)
            {
                bool b = false;
                foreach (var sp in _context.SpyNews.Where(c => c.Charter == Charter.Political).ToList())
                {
                    if (sp.Link==item.Link)
                    {
                        b = true;
                        break;
                    }
                }
                if (!b)
                {
                    {
                        _context.SpyNews.AddRange(
                                           new SpyNews
                                           {
                                               Link = item.Link,
                                               Description = item.Description,
                                               Title = item.Title,
                                               PubDate = item.PubDate,
                                               Charter = Charter.Political
                                           });
                        _context.SaveChanges();
                    }
                }
                else
                {
                    break;
                }
               
            }
            my.Clear();





        }

        public class MainSheduleTask : IHostedService
        {
            private Task _executingTask;
            private CancellationTokenSource _cts;
            private IServiceProvider _services { get; }
            // run cheking of the sheduled tasks every 1 minute or 10 minutes.
            public int MainSheduleTimeSec { get; set; } = 60; //10*60; 


            public MainSheduleTask(IServiceProvider services)
            {
                _services = services;
            }

            public async Task StopAsync()
            {
                await StopAsync(_cts.Token);
            }

            public Task StartAsync(CancellationToken cancellationToken)
            {
                // Create a linked token so we can trigger cancellation outside of this token's cancellation
                _cts = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

                // Store the task we're executing
                _executingTask = ExecuteAsync(_cts.Token);

                // If the task is completed then return it, otherwise it's running
                return _executingTask.IsCompleted ? _executingTask : Task.CompletedTask;
            }

            public async Task StopAsync(CancellationToken cancellationToken)
            {
                // Stop called without start
                if (_executingTask == null)
                {
                    return;
                }

                // Signal cancellation to the executing method
                _cts.Cancel();

                // Wait until the task completes or the stop token triggers
                await Task.WhenAny(_executingTask, Task.Delay(-1, cancellationToken));

                // Throw if cancellation triggered
                cancellationToken.ThrowIfCancellationRequested();
            }

            // Derived classes should override this and execute a long running method until 
            // cancellation is requested
            public async Task ExecuteAsync(CancellationToken cancellationToken)
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    await Task.Delay(TimeSpan.FromSeconds(MainSheduleTimeSec), cancellationToken);
                    using (var scope = _services.CreateScope())
                    {
                        var scopedProcessingService =
                            scope.ServiceProvider
                                .GetRequiredService<SheduledTaskService>();

                        await scopedProcessingService.DoWork();
                    }
                }
            }


        }
    }
}
