﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace UBY_NewsPortal.Models.NewsViewModels
{
    public enum Charter
    {

        Political = 1,
        Economic = 2,
        Sport = 4,
        Science = 8,
        Culture = 16,
        Society=32,
        Armenia=64,
        World=128,
        Region=256,
        Artsakh=512,
    }
    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime PubDate { get; set; }
        public string Content { get; set; }
        public Charter Charter { get; set; }
        public string Link { get; set; }

        [NotMapped]
        [Display(Name = "Economic")]
        public bool? Economic
        {
            get { return (Charter & Charter.Economic) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Economic; }
                else { Charter &= (~Charter.Economic); }
            }
        }
        [NotMapped]
        [Display(Name = "Political")]
        public bool? Political
        {
            get { return (Charter & Charter.Political) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Political; }
                else { Charter &= (~Charter.Political); }
            }
        }
        [NotMapped]
        [Display(Name = "Sport")]
        public bool? Sport
        {
            get { return (Charter & Charter.Sport) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Sport; }
                else { Charter &= (~Charter.Sport); }
            }
        }
        [NotMapped]
        [Display(Name = "Science")]
        public bool? Science
        {
            get { return (Charter & Charter.Science) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Science; }
                else { Charter &= (~Charter.Science); }
            }
        }
        [NotMapped]
        [Display(Name = " Culture")]
        public bool? Culture
        {
            get { return (Charter & Charter.Culture) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Culture; }
                else { Charter &= (~Charter.Culture); }
            }
        }
        [NotMapped]
        [Display(Name = " Society")]
        public bool? Society
        {
            get { return (Charter & Charter.Society) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Society; }
                else { Charter &= (~Charter.Society); }
            }
        }
        [NotMapped]
        [Display(Name = " Armenia")]
        public bool? Armenia
        {
            get { return (Charter & Charter.Armenia) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Armenia; }
                else { Charter &= (~Charter.Armenia); }
            }
        }
        [NotMapped]
        [Display(Name = " World")]
        public bool? World
        {
            get { return (Charter & Charter.World) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.World; }
                else { Charter &= (~Charter.World); }
            }
        }
        [NotMapped]
        [Display(Name = " Region")]
        public bool? Region
        {
            get { return (Charter & Charter.Region) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Region; }
                else { Charter &= (~Charter.Region); }
            }
        }
        [NotMapped]
        [Display(Name = " Artsakh")]
        public bool? Artsakh
        {
            get { return (Charter & Charter.Artsakh) > 0; }
            set
            {
                if (value == true) { Charter |= Charter.Artsakh; }
                else { Charter &= (~Charter.Artsakh); }
            }
        }
    }
}
